package Listener;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;

public class GameKeyListener implements KeyListener{
	
	public void keyPressed(KeyEvent e) {}

	public void keyReleased(KeyEvent e) {}

	public void keyTyped(KeyEvent e) {
		if(e.getKeyChar() > '0' && e.getKeyChar() <= '9' ) {
			((JButton)e.getSource()).setText(String.valueOf(e.getKeyChar()));
		}
	}
	
}
