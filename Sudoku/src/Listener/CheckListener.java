package Listener;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import Model.Sudoku;

public class CheckListener implements ActionListener{
	private int size;
	private Sudoku sudoku;
	private JButton[][] grid;
	
	public CheckListener(int size, Sudoku sudoku, JButton[][] grid) {
		super();
		this.size = size;
		this.sudoku = sudoku;
		this.grid = grid;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		int count = 0;
		for(int i = 0; i < this.size; i++) {
			for(int j = 0; j < this.size; j++) {
				if(!grid[i][j].getText().equals("")) {
					if(this.sudoku.getGrid()[i][j] == Integer.parseInt((grid[i][j].getText().trim()))) {
						grid[i][j].setBackground(Color.GREEN);
						grid[i][j].setEnabled(false);
						count++;
					}else {
						grid[i][j].setBackground(Color.RED);
					}
				}
			}	
		}
		if(count == this.size*this.size) {
			JOptionPane.showMessageDialog(null, "Vous avez gagn� !", "Victoire", JOptionPane.INFORMATION_MESSAGE);
		}
	}

}
