package Listener;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

import View.Interface;

public class SizeKeyListener implements KeyListener{

	private Interface ui;
	private int size;

	public SizeKeyListener(Interface ui, int size) {
		super();
		this.ui = ui;
		this.size = size;
	}
	
	
	@Override
	public void keyPressed(KeyEvent e) {}

	@Override
	public void keyReleased(KeyEvent e) {}

	@Override
	public void keyTyped(KeyEvent e) {
		if (e.getKeyChar() == KeyEvent.VK_ENTER) {

			try {
				String input = ((JTextComponent) e.getSource()).getText();
				if(input.matches("-?\\d+")) {
					double value = Math.sqrt(Double.parseDouble(input));
					int intValue = (int) Math.sqrt(Double.parseDouble(input));

					if (value == intValue) {
						int size = Integer.parseInt(((JTextField) e.getSource()).getText());
						this.ui.remakeUI(size);	
					}	
				}
				
			} catch (NumberFormatException nfe) {
				// do Nothing
			}

		}
	}

}
