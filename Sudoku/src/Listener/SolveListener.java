package Listener;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import Model.Sudoku;

public class SolveListener implements ActionListener {

	private int size;
	private JButton[][] grid;
	private Sudoku sudoku;
	
	public SolveListener(int size, Sudoku sudoku, JButton[][] grid) {
		super();
		this.size = size;
		this.grid = grid;
		this.sudoku = sudoku;
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		int count = 0;
		for(int i = 0; i < this.size; i++) {
			for(int j = 0; j < this.size; j++) {
				grid[i][j].setEnabled(false);
				grid[i][j].setBackground(Color.RED);
				grid[i][j].setText(String.valueOf(this.sudoku.getGrid()[i][j]));
			}	
		}
		if(count == this.size*this.size) {
			JOptionPane.showMessageDialog(null, "Vous avez gagn� !", "Victoire", JOptionPane.INFORMATION_MESSAGE);
		}else {
			JOptionPane.showMessageDialog(null, "Vous avez perdu", "D�faite", JOptionPane.INFORMATION_MESSAGE);
		}
	}

}
