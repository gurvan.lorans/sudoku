package View;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JTextField;

public class PlaceHolderTextField extends JTextField implements FocusListener{

	private String content;
	
	public PlaceHolderTextField(String content) {
		super(content);
		this.content = content;
		this.addFocusListener(this);
	}
	
	@Override
	public void focusGained(FocusEvent e) {
		if (((JTextField) e.getSource()).getText().equals(this.content)) {
			((JTextField) e.getSource()).setText("");
			((JTextField) e.getSource()).setForeground(Color.BLACK);
		}
	}

	@Override
	public void focusLost(FocusEvent e) {
		if (((JTextField) e.getSource()).getText().isEmpty()) {
			((JTextField) e.getSource()).setForeground(Color.GRAY);
			((JTextField) e.getSource()).setText(this.content);
		}
	}

}
