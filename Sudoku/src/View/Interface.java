package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import Listener.CheckListener;
import Listener.GameKeyListener;
import Listener.SizeKeyListener;
import Listener.SolveListener;
import Model.Sudoku;

public class Interface extends JFrame {

	private JButton[][] grid;
	private JPanel[] squares;
	private JPanel game;
	private JPanel param;
	private JPanel main;

	private JButton chosen;
	private JButton check;
	private JButton solve;

	private int size;
	private Sudoku sudoku;
	private PlaceHolderTextField sizeTF;
	
	private Color blackColor;
	private Color greyColor;
	private Color orangeColor;
	private Color whiteColor;

	public Interface(int size) {
		super();
		remakeUI(size);
		this.pack();
		this.setTitle("Sudoku");
		this.setVisible(true);
		this.setLocationRelativeTo(null);

	}

	public void remakeUI(int size) {
		this.blackColor = new Color(34, 40, 49);
		this.greyColor = new Color(57, 62, 70);
		this.orangeColor = new Color(214, 90, 49);
		this.whiteColor = new Color(238, 238, 238);
		
		this.getContentPane().removeAll();
		this.size = size;
		
		this.grid = new JButton[size][size];
		this.squares = new JPanel[size];
		
		try {
			this.sudoku = new Sudoku(this.size, this.size);
		} catch (Exception e) {
			e.printStackTrace();
		}

		initContent();
		addContent();
		this.chosen = this.grid[0][0];
		this.getContentPane().revalidate();
		this.getContentPane().repaint();

		this.pack();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	public void initContent() {
		UIManager.put("Button.disabledText", Color.BLACK);
		// Main Panel
		this.main = new JPanel();
		this.main.setLayout(new BorderLayout());
		this.main.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		this.main.setBackground(this.blackColor);
		
		// Right pane (to chose number and change size of the grid)
		this.param = new JPanel();
		this.param.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		this.param.setLayout(new BorderLayout());
		this.param.setOpaque(false);
		
		// The button to check the numbers sets
		this.check = new JButton("V�rifier");
		this.check.addActionListener(new CheckListener(this.size, this.sudoku, this.grid));
		
		// The button to solve the Sudoku
		this.solve = new JButton("Finir");
		this.solve.addActionListener(new SolveListener(this.size, this.sudoku, this.grid));

		// The JTextField to change the size
		this.sizeTF = new PlaceHolderTextField("Taille de la grille");
		this.sizeTF.setForeground(Color.GRAY);
		this.sizeTF.addKeyListener(new SizeKeyListener(this, this.size));

		// The panel for the game's JButton
		this.game = new JPanel();
		this.game.setOpaque(false);
		this.game.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		GridLayout gl = new GridLayout((int)Math.sqrt(this.size), (int)Math.sqrt(this.size));
		this.game.setLayout(gl);

		for(int i = 0; i < this.size; i++) {
			this.squares[i] = new JPanel();
			this.squares[i].setLayout(gl);
			this.squares[i].setOpaque(false);
			this.squares[i].setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		}
		
		for (int i = 0; i < this.size; i++) {
			for (int j = 0; j < this.size; j++) {
				initButtonGame(i, j);
			}
		}
	}
	
	public int[][] makeHideGrid() {
		int[][] hideGrid = new int[this.size][this.size];
		
		for(int i = 0; i < this.size; i++) {
			hideGrid[i]	= this.sudoku.getGrid()[i].clone();
		}
		
		// Striking out
		for (int k1 = 0; k1 < this.size; k1++) {
			for (int k2 = 0; k2 < this.size; k2++)
				hideGrid = this.sudoku.hideValues(hideGrid, k1, k2);
		}
		return hideGrid;
	}
	
	public void initButtonGame(int i, int j) {
		
		// We initialize the JButton
		int [][] hideGrid = makeHideGrid();
		this.grid[i][j] = new JButton();
		this.grid[i][j].setBackground(this.greyColor);
		this.grid[i][j].setPreferredSize(new Dimension(40, 40));
		
		// We fix some numbers
		if(hideGrid[i][j] > 0) {
			this.grid[i][j].setBackground(Color.GREEN);
			this.grid[i][j].setEnabled(false);
			this.grid[i][j].setText(String.valueOf(hideGrid[i][j]));
		}
		
		// We make an ActionListener to get the chosen JButton
		this.grid[i][j].addActionListener(e -> this.chosen = (JButton) e.getSource());
		this.grid[i][j].addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent e) {
				((JButton) e.getSource()).setBackground(new Color(238, 238, 238));
			}

			public void focusLost(FocusEvent e) {
				((JButton) e.getSource()).setBackground(new Color(57, 62, 70));
			}
		});
		
		// We add a Key Listener to get input from the KeyBoard
		this.grid[i][j].addKeyListener(new GameKeyListener());
	}

	public void addContent() {
		
		
		for (int i = 0; i < this.size; i++) {
			for (int j = 0; j < this.size; j++) {

				int root = (int)Math.sqrt(this.size);
				int squareX = (int)(j/root);
				int squareY = (int)(i/root);
				int index = squareX+(squareY*root);
				
				System.out.print(index);
				
				this.squares[index].add(this.grid[i][j]);
			}
			System.out.println();
		}
		
		for(int i = 0; i < this.size; i++) {
			this.game.add(this.squares[i]);
		}
		this.param.add(sizeTF, BorderLayout.NORTH);

		this.param.add(new Choose(this.size, this), BorderLayout.CENTER);

		JPanel buttons = new JPanel();
		buttons.add(check);
		buttons.add(solve);
		this.param.add(new Choose(this.size, this), BorderLayout.CENTER);
		this.param.add(buttons, BorderLayout.SOUTH);

		this.main.add(game, BorderLayout.CENTER);
		this.main.add(param, BorderLayout.EAST);
		this.setContentPane(this.main);
	}

	public JButton getChosen() {
		return this.chosen;
	}

	public static void main(String[] args) {
		new Interface(9);
	}
}
