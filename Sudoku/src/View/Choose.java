package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Choose extends JPanel{
	
	private JButton[] numbers;
	private JButton reset;
	private JPanel choosePane;
	
	private Color blackColor;
	private Color greyColor;
	private Color orangeColor;
	private Color whiteColor;
	
	public Choose(int size, Interface ui) {
		super();
		
		initContent(size);
		addListenerButton(size, ui);

		this.setOpaque(false);
		this.setLayout(new BorderLayout());
		this.add(this.choosePane, BorderLayout.CENTER);
		this.add(this.reset, BorderLayout.SOUTH);
		
		this.setVisible(true);
	}
	
	
	public void initContent(int size) {
		
		this.blackColor = new Color(34, 40, 49);
		this.greyColor = new Color(57, 62, 70);
		this.orangeColor = new Color(214, 90, 49);
		this.whiteColor = new Color(238, 238, 238);
		
		
		this.choosePane = new JPanel();
		this.choosePane.setOpaque(false);
		this.choosePane.setLayout(new GridLayout((int)Math.sqrt(size), (int)Math.sqrt(size), 10, 10));
		this.choosePane.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
		
		this.reset = new JButton();
		this.reset.setPreferredSize(new Dimension(50, 50));
		
		this.numbers = new JButton[size];
		this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
	}
	
	public void addListenerButton(int size, Interface ui) {
		for(int i = 0; i < size; i++) {
			numbers[i] = new JButton(String.valueOf(i+1));
			numbers[i].addActionListener(e -> {
				if(ui.getChosen() != null) {
					ui.getChosen().setText(((JButton) e.getSource()).getText());
				}
			});
			this.numbers[i].setBorderPainted(false);
			this.numbers[i].setFocusPainted(false);
			this.numbers[i].setContentAreaFilled(false);
			this.numbers[i].setOpaque(true);
			this.numbers[i].setBackground(this.orangeColor);
			
			this.choosePane.add(numbers[i]);
		}

		this.reset.setBorderPainted(false);
		this.reset.setFocusPainted(false);
		this.reset.setContentAreaFilled(false);
		this.reset.setOpaque(true);
		this.reset.setBackground(this.orangeColor);
		this.reset.addActionListener(e -> {
			if(ui.getChosen() != null) {
				ui.getChosen().setText("");
			}
		});
		
	}
	
}
