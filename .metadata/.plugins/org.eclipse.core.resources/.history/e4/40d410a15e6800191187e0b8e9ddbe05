package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import Listener.CheckListener;
import Listener.GameKeyListener;
import Listener.SizeKeyListener;
import Listener.SolveListener;
import Model.Sudoku;

public class Interface extends JFrame {

	private JButton[][] grid;
	private JPanel[] squares;
	private JPanel game;
	private JPanel param;
	private JPanel main;

	private JButton chosen;
	private JButton check;
	private JButton solve;

	private int size;
	private Sudoku sudoku;
	private PlaceHolderTextField sizeTF;

	public Interface(int size) {
		super();
		remakeUI(size);
		this.pack();
		this.setTitle("Sudoku");
		this.setVisible(true);
		this.setLocationRelativeTo(null);

	}

	public void remakeUI(int size) {
		this.getContentPane().removeAll();
		this.size = size;
		this.grid = new JButton[size][size];
		this.squares = new JPanel[size];

		try {
			this.sudoku = new Sudoku(this.size, this.size);
		} catch (Exception e) {
			e.printStackTrace();
		}

		initContent();
		addContent();
		this.chosen = this.grid[0][0];
		this.getContentPane().revalidate();
		this.getContentPane().repaint();

		this.pack();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	public void initContent() {
		UIManager.put("Button.disabledText", Color.BLACK);
		this.main = new JPanel();
		this.main.setLayout(new BorderLayout());
		this.main.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		this.param = new JPanel();
		this.param.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		this.param.setLayout(new BorderLayout());
		this.check = new JButton("V�rifier");
		this.check.addActionListener(new CheckListener(this.size, this.sudoku, this.grid));
		this.solve = new JButton("Finir");
		this.solve.addActionListener(new SolveListener(this.size, this.sudoku, this.grid));

		this.sizeTF = new PlaceHolderTextField("Taille de la grille");
		this.sizeTF.setForeground(Color.GRAY);

		this.sizeTF.addKeyListener(new SizeKeyListener(this, this.size));

		this.game = new JPanel();
		this.game.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		this.game.setLayout(new GridLayout(this.size, this.size));

		for (int i = 0; i < this.size; i++) {
			for (int j = 0; j < this.size; j++) {
				initButtonGame(i, j);
			}
		}
		
		for(int i = 0; i < this.size; i++) {
			this.squares[i] = new JPanel();
			this.squares[i].setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		}
		
	}
	
	public int[][] makeHideGrid() {
		int[][] hideGrid = new int[this.size][this.size];
		
		for(int i = 0; i < this.size; i++) {
			hideGrid[i]	= this.sudoku.getGrid()[i].clone();
		}
		
		// Striking out
		for (int k1 = 0; k1 < this.size; k1++) {
			for (int k2 = 0; k2 < this.size; k2++)
				hideGrid = this.sudoku.hideValues(hideGrid, k1, k2);
		}
		return hideGrid;
	}
	
	public void initButtonGame(int i, int j) {
		
		// We initialize the JButton
		int [][] hideGrid = makeHideGrid();
		this.grid[i][j] = new JButton();
		this.grid[i][j].setBackground(Color.GRAY);
		this.grid[i][j].setPreferredSize(new Dimension(40, 40));
		
		// We fix some numbers
		if(hideGrid[i][j] > 0) {
			this.grid[i][j].setBackground(Color.GREEN);
			this.grid[i][j].setEnabled(false);
			this.grid[i][j].setText(String.valueOf(hideGrid[i][j]));
		}
		
		// We make an ActionListener to get the chosen JButton
		this.grid[i][j].addActionListener(e -> this.chosen = (JButton) e.getSource());
		this.grid[i][j].addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent e) {
				((JButton) e.getSource()).setBackground(Color.BLUE);
			}

			public void focusLost(FocusEvent e) {
				((JButton) e.getSource()).setBackground(Color.GRAY);
			}
		});
		
		// We add a Key Listener to get input from the KeyBoard
		this.grid[i][j].addKeyListener(new GameKeyListener());
	}

	public void addContent() {
		
		for (int i = 0; i < this.size; i++) {
			for (int j = 0; j < this.size; j++) {
				
//				int squareX = i%(int)Math.sqrt(this.size);
//				int squareY = j%(int)Math.sqrt(this.size);
//				System.out.println(i+squareY);
				int root = (int)(Math.sqrt(this.size));
				int squareX = (int)(i/root)*root;
				int squareY = (int)(j/root);
				int index = squareX+squareY;
//				0/3 = 0	  1/3 = 0   2/3 = 0   3/3 = 1   4/3 = 1   5/3 = 1   6/3 = 2   7/3 = 2   8/3 = 2
//				0         1         2         3         4         5         6         7			8
//				012345678 012345678 012345678 012345678 012345678 012345678 012345678 012345678 012345678
//				000111222 000111222 000111222 333444555 333444555 333444555 666777888 666777888 666777888
				
				this.squares[i].add(this.grid[i][j]);
			}
			System.out.println();
		}
		
		for(int i = 0; i < this.size; i++) {
			this.game.add(squares[i]);	
		}
		
		
		
//		|1|1|1|  |2|2|2|  |3|3|3|
//		|1|1|1|  |2|2|2|  |3|3|3|
//		|1|1|1|  |2|2|2|  |3|3|3|
//		
//		|4|4|4|  |5|5|5|  |6|6|6|
//		|4|4|4|  |5|5|5|  |6|6|6|
//		|4|4|4|  |5|5|5|  |6|6|6|
//
//		|7|7|7|  |8|8|8|  |9|9|9|
//		|7|7|7|  |8|8|8|  |9|9|9|
//		|7|7|7|  |8|8|8|  |9|9|9|
		
		
		
		this.param.add(sizeTF, BorderLayout.NORTH);

		this.param.add(new Choose(this.size, this), BorderLayout.CENTER);

		JPanel buttons = new JPanel();
		buttons.add(check);
		buttons.add(solve);
		this.param.add(new Choose(this.size, this), BorderLayout.CENTER);
		this.param.add(buttons, BorderLayout.SOUTH);

		this.main.add(game, BorderLayout.CENTER);
		this.main.add(param, BorderLayout.EAST);
		this.setContentPane(this.main);
	}

	public JButton getChosen() {
		return this.chosen;
	}

	public static void main(String[] args) {
		new Interface(9);
	}
}
